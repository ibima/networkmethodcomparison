HyperGeomTest<-function(glDraw, glSet, glUniv, runID='test', runDesc='test-1'){
  if(any(! c(glDraw, glSet) %in% glUniv)) stop('glUniv should have everything...!')
  gi<-unique(intersect(glDraw, glSet))
  numWdrawn <- length(gi)
  numW <- length(unique(glSet))
  numALL <- length(unique(glUniv))
  numB <- numALL - numW
  numDrawn <- length(unique(glDraw))
  p<-phyper(numWdrawn, numW, numB, numDrawn, lower.tail = FALSE)
  df<-data.frame(ID=runID, Description=runDesc, 
                 numWdrawn=numWdrawn, numDrawn=numDrawn, GeneRatio=paste0(numWdrawn, '/', numDrawn),
                 numW=numW, numALL=numALL, BgRatio=paste0(numW, '/', numALL), pvalue=p, pAdjust=NA, qvalue=NA, stringsAsFactors=F)
  return(list(df=df, gi=gi))
} 