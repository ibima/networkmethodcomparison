#!perl

use strict;

my ($firstline,$lastline) = @ARGV;

open DISFILE , "<CUI2Disease.csv" or die;
open TMPFILE , "+>TmpFile.csv" or die;

while ( <DISFILE> ) {
	chomp;

	if ( $. >= $firstline && $. <= $lastline ) {
		print TMPFILE $_ , "\n";
	}
}

system( "Rscript ReadTmpFile.R" )


