# Auxillary files for Network Method Comparison

This repository provides R code that implemented the comparison of methods in
their ability to identify disease-associated genes by changes observed in the
transcriptome. The analysis was published in

FocusHeuristics – expression-data-driven network optimization and disease gene prediction
Mathias Ernst, Yang Du, Gregor Warsow, Mohamed Hamed, Nicole Endlich, Karlhans Endlich, Hugo Murua Escobar, Lisa-Madeleine Sklarz, Sina Sender, Christian Junghanß, Steffen Möller, Georg Fuellen & Stephan Struckmann
Scientific Reports volume 7, Article number: 42638 (2017)
doi:10.1038/srep42638
https://www.nature.com/articles/srep42638



	all_gene_disease_associations.csv - File from DisGeneNet
	all_gene_disease_associations.csv.parsed.csv
	all_gene_disease_associations.txt
	benchmark_functions.R - from Yang
	benchmark.geo.disgene_orig.Rmd
	benchmark.geo.disgenet.R
	benchmark.geo.disgenet.Rmd
	checkDatasets.R  - some temporary code, following curiosity on data structure
	CUI2Disease.csv - CUI are disease identifier, derived from DisGeneNet
	curated_gene_disease_associations.txt - from DisGeneNet
	DataOverview.csv - table also found as a supplement to the paper
	DetailsForSingleGEO.R - 
	DisGeneAllTab.csv - whole DisGeneNet data
	FindBrokenEntry.pl - investigate why R did not want to import some records
	getExpressionStats.R - retrieves expression data for particular experiment/disease/whatever from big table
	HubGeneTable.csv - hub genes defined ourselves from number of pleiotropic associations
	PairsCode.R - figure in paper as pairs plot
	PaperRevisionAddCode.R - additional effort performed for revision
	parseDisGenNet.pl
	parseDisGenNet.R
	stuff_17_3.R - some temporary stuff
	Workspaces


Original files on Mathias's workstation on /Home/Dokumenta/IBIMA/VIPproject/DisGenNet
