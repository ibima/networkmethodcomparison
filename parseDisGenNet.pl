#!perl 

use strict;

my $file = "all_gene_disease_associations.csv";
my $outfile = $file . ".parsed.csv";

open INF , "<$file" or die;
open OUTF , "+>$outfile" or die;

while ( <INF> ) {
	chomp;
	my $line = $_;
	$line =~ s/umls:(.+?);/$1;/;
	my @line = split /;/ , $line ;

	my @asstypes = split /,/ , $line[5];
	print OUTF $line . ";" . $_ . "\n" for @asstypes;  
	#sleep 1;

}
