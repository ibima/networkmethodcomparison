library(limma)

nephro.cui = "C0011881"
nephro.geo = SampleSizes$geoIndex[ SampleSizes$CUI == nephro.cui ]

my.geo = 56
#my.geo = nephro.geo

###########
X = X[ X$qseq <= 0.01 , ]
###########

Z = X[ X$geo.index == my.geo , ]
Z.real = Z[ Z$CUI == Z$RealCUI , ]

cui = names(ll)[my.geo]
cui.disease = unique( Z.real$RealDisease )

l = "FocusHeuristics"
plot( 0 , xlim= c(0,max(Z.real$FPR.nc) ) , ylim=c(0,max(Z.real$TPR.nc) ) , type="n",xlab="FPR",ylab="TPR" ,
      main = paste( my.geo , unique( Z.real$RealCUI ) , unique( Z.real$RealDisease ) , sep="." )  )
points( Z.real$FPR.nc[ Z.real$method == l ] , Z.real$TPR.nc[ Z.real$method == l ] , type="p",col="green",cex=0.5 )
text( Z.real$FPR.nc[ Z.real$method == l ] , Z.real$TPR.nc[ Z.real$method == l ] + 0.01 ,
      labels = paste( round(Z.real$qseq[ Z.real$method == l ],4) , Z.real$picks[ Z.real$method == l ] ) , cex = 0.3 )

l = unique( Z.real$method )
for ( i in 1:length(l) ) {
  points( Z.real$FPR.nc[ Z.real$method == l[i] ] , Z.real$TPR.nc[ Z.real$method == l[i] ] , type="l",col="grey",lwd=0.6 )
}        

#geo.index = nephro.geo
geo.index = 56

set = ll[[ geo.index ]]
set.df = set$table
cui = names(ll)[ geo.index ]

cui.name = disgene.names[cui]

xx = as.matrix( set.df[ , c( set$ctrl , set$trmt ) ] )  
rownames(xx) = set.df$IDENTIFIER
rownames(xx) = toupper( rownames(xx) )

sum.na = apply( xx , 1 , function (x) { sum(is.na(x)) } )
xx = xx[ ! sum.na == ncol(xx) , ]  
xx[ is.na(xx) ] = 0

design.fac = factor( c( rep( "ctrl" , length(set$ctrl) ) , rep( "disease" , length(set$trmt)  ) ) )

if ( sum(xx >= 30) > 500 ) { xx = log2( xx + 1 ) } 
xx = xx + rnorm( prod(dim(xx)) , mean=0 , sd=0.001)

colnames(xx) = design.fac

my.pca = prcomp( t(xx))
tp = t.test( my.pca$x[ design.fac == "ctrl" ,1] , my.pca$x[ design.fac == "disease" , 1 ] )$p.value

xx2 = aggregate( xx , by = list( rownames(xx) ) , mean )
xx = as.matrix( xx2[ , 2:ncol(xx2) ] )
rownames(xx) = xx2$Group.1

chipGenes = rownames(xx)
gene.universe = intersect( chipGenes , V(netGraph)$name ) 

design = model.matrix( ~ design.fac )
colnames(design) = c("intercept","disease")
graphFromIdx<-match(netTab[,'Source_Symbol'], chipGenes)
graphToIdx<-match(netTab[,'Target_Symbol'], chipGenes)
okg<-!apply(cbind(graphFromIdx, graphToIdx), 1, function(x) any(is.na(x)))

inputGraph<-graph.edgelist(as.matrix(netTab[okg,][, c('Source_Symbol', 'Target_Symbol')]))

DisGenTab.list.cp = DisGenTab.list
DisGenTab.list.cp = lapply( DisGenTab.list.cp , function (l) {
  l[ l %in% gene.universe ]    
})

DisGenTab.nocommons.list.cp = DisGenTab.nocommons.list
DisGenTab.nocommons.list.cp = lapply( DisGenTab.nocommons.list.cp , function (l) {
  l[ l %in% gene.universe ]    
})


L = list( data.frame( "type" = "all" , "gene" = DisGenTab.list[cui] ) , 
          data.frame( "type" = "no commons" , "gene" = DisGenTab.nocommons.list[cui] ) )

L = do.call( rbind , L )
write.table( L , file = paste("DisGeNet_",cui,".txt",sep="") , sep=";" , quote=F  )

scores <- prepLISLFC.original(xx, trmtIdx=design.fac == "disease", graphFromIdx=graphFromIdx[okg],
                              graphToIdx=graphToIdx[okg], graphEdgeSign=graphEdgeSign[okg])

qseq[1:5]
topQ = 0.0003061224

seedTagsIdx<-doSeed(scores, is.q = topQ, lfc.q = topQ, ls.q.low = topQ/2, ls.q.upp = topQ/2)
seedTags<- unique( c(unlist(netTab[okg,][seedTagsIdx$eI, c('Source_Symbol', 'Target_Symbol')]), rownames(xx)[seedTagsIdx$vI]) )
netTags <- seedTags[seedTags %in% unique(unlist(netTab[okg,][, c('Source_Symbol', 'Target_Symbol')]))]
seedG = induced.subgraph( inputGraph , vids = V(inputGraph)[ V(inputGraph)$name %in% netTags ] )


seedG = simplify( as.undirected( seedG ) )

V(seedG)$color = "grey"

V(seedG)$color[ V(seedG)$name %in% DisGenTab.list[[cui]] ] = "red"

plot(seedG , vertex.size=4 , vertex.label.cex=0.5 )

gml.filename = paste(cui,".gml",sep="")

write.graph( seedG , format = "gml" , file = gml.filename )



fit1 <- lmFit(xx , design)  # fit each probeset to model
efit <- eBayes(fit1)        # empirical Bayes adjustment
topTab<-topTable(efit, coef= "disease", number = nrow(xx), sort.by="p") 

topTab = topTab[ rownames(topTab) %in% gene.universe , ]

limma.genes = rownames(topTab)
limma.genes = limma.genes[ limma.genes %in% V(inputGraph)$name ]

list.of.lrs.genes = list( "a" = limma.genes[1:100] , "b" = limma.genes[1:50] )  
lrs.ssps <- rep(0,vcount(inputGraph))
a.limma = abs(topTab$logFC)
names(a.limma) = rownames(topTab)
list.of.lrs.genes = lapply( list.of.lrs.genes , function (l) { a.limma[ l ] } )



print("start LRS")
lrs.ssps = sapply( 1:vcount(inputGraph) , function (i) {
  mean( shortest.paths(inputGraph, V(inputGraph)$name[i], names(list.of.lrs.genes$b),mode="all" ) / list.of.lrs.genes$b)  
})
names(lrs.ssps) = V(inputGraph)$name

lrs.ssps.decr = sort( lrs.ssps , decreasing = T )
lrs.ssps.decr = lrs.ssps.decr[ ! is.infinite( lrs.ssps.decr ) ]
lrs.ssps.incr = sort( lrs.ssps , decreasing = F )
print("LRS finished")


n = vcount( seedG )

genes.focus = V(seedG)$name
genes.LRS = names( lrs.ssps.incr )[1:n]
#genes.goldstandard = DisGenTab.list[[cui]]
genes.goldstandard = DisGenTab.nocommons.list[[cui]]


genes = unique( c( genes.focus , genes.LRS , genes.goldstandard ) )

Z = cbind( genes %in% genes.focus , genes %in% genes.LRS , genes %in% genes.goldstandard )
colnames(Z) = c("FocusHeuristics","LRS","GoldStandard")

vennDiagram( Z )

list.of.genes = list( 
  "FocusHeuristics" = V(seedG)$name ,
  "LRS" = genes.LRS , 
  "gold standard full" = DisGenTab.list[[cui]] ,
  "gold standard, no common genes" = DisGenTab.nocommons.list[[cui]] , 
  "gold standard full, found by FocusH" = intersect( genes.focus , DisGenTab.list[[cui]] ) ,
  "gold standard full, found by LRS" = intersect( genes.LRS , DisGenTab.list[[cui]] ) , 
  "gold standard ncg, found by FocusH" = intersect( genes.focus , DisGenTab.nocommons.list[[cui]] ) ,
  "gold standard ncg, found by LRS" = intersect( genes.LRS , DisGenTab.nocommons.list[[cui]] ) )


sink( "AMKL_genes_FocusH_LRS.txt" )
print( list.of.genes )
sink()




